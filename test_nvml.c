#include <stdio.h>
#include <stdlib.h>
#include <nvml.h>
#include <time.h>
#include <unistd.h>
#include <math.h>

#define SECONDS_TO_MICROSECONDS_MULT 1000000
#define NANOSECONDS_TO_MICROSECONDS_DIV 1000
#define NUM_EXEC_DEFAULT 10000

void check_nvml_error(nvmlReturn_t result, const char *message) {
    if (result != NVML_SUCCESS) {
        fprintf(stderr, "Error: %s: %s\n", message, nvmlErrorString(result));
        exit(1);
    }
}

int compare_longs(const void *a, const void *b) {
    return (*(long*)a - *(long*)b);
}

void compute_statistics(long *data, int size, double *avg, long *min, long *max, double *std, long *q1, long *q3) {
    long sum = 0;
    *min = data[0];
    *max = data[0];

    for (int i = 0; i < size; i++) {
        if (data[i] < *min) *min = data[i];
        if (data[i] > *max) *max = data[i];
        sum += data[i];
    }

    *avg = (double)sum / size;

    double variance = 0;
    for (int i = 0; i < size; i++) {
        variance += pow(data[i] - *avg, 2);
    }
    *std = sqrt(variance / size);

    qsort(data, size, sizeof(long), compare_longs);
    *q1 = data[size / 4];
    *q3 = data[3 * size / 4];
}

int main(int argc, char *argv[]) {
    int loop_count = (argc == 2) ? atoi(argv[1]) : 100;
    if (loop_count <= 0) {
        fprintf(stderr, "Error: The number of iterations must be a positive integer.\n");
        return 1;
    }

    nvmlReturn_t result;
    unsigned int deviceCount;
    struct timespec start, end;
    long time_diff_us;

    // Initialize NVML library
    result = nvmlInit();
    check_nvml_error(result, "Failed to initialize NVML");

    // Get hardware info
    char hostname[1024];
    gethostname(hostname, 1024);
    printf("Hostname: %s\n", hostname);
    result = nvmlDeviceGetCount(&deviceCount);
    check_nvml_error(result, "Failed to get device count");
    printf("Number of GPUs: %u\n", deviceCount);

    nvmlDevice_t devices[deviceCount];
    long *time_diffs[deviceCount];
    char *serials[deviceCount];

    for (unsigned int j = 0; j < deviceCount; j++) {
        // Get handle
        result = nvmlDeviceGetHandleByIndex(j, &devices[j]);
        check_nvml_error(result, "Failed to get handle for device");
        // Get name
        char name[NVML_DEVICE_NAME_BUFFER_SIZE];
        result = nvmlDeviceGetName(devices[j], name, NVML_DEVICE_NAME_BUFFER_SIZE);
        check_nvml_error(result, "Failed to get name of device");        
        // Get serial
        serials[j] = (char *)malloc(NVML_DEVICE_SERIAL_BUFFER_SIZE * sizeof(char));
        if (serials[j] == NULL) {
            fprintf(stderr, "Error: Failed to allocate memory for serials.\n");
            return 1;
        }
        result = nvmlDeviceGetSerial(devices[j], serials[j], NVML_DEVICE_SERIAL_BUFFER_SIZE);
        check_nvml_error(result, "Failed to get serial of device");
        printf("GPU %u: %s, serial: %s\n", j, name, serials[j]);
        // Allocate memory for time_diffs
        time_diffs[j] = (long *)malloc(loop_count * sizeof(long));
        if (time_diffs[j] == NULL) {
            fprintf(stderr, "Error: Failed to allocate memory for time_diffs.\n");
            return 1;
        }
    }

    printf("\nTesting nvmlDeviceGetTotalEnergyConsumption ...\n");
    for (unsigned int j = 0; j < deviceCount; j++) {
        for (int i = 0; i < loop_count; i++) {
            unsigned long long energy;

            clock_gettime(CLOCK_MONOTONIC, &start);
            result = nvmlDeviceGetTotalEnergyConsumption(devices[j], &energy);
            clock_gettime(CLOCK_MONOTONIC, &end);

            check_nvml_error(result, "Failed to get total energy consumption");
            time_diff_us = (end.tv_sec - start.tv_sec) * SECONDS_TO_MICROSECONDS_MULT 
                            + (end.tv_nsec - start.tv_nsec) / NANOSECONDS_TO_MICROSECONDS_DIV;
            time_diffs[j][i] = time_diff_us;
        }
    }

    // Write to CSV file
    char filename[1024];
    char date[20];
    time_t now = time(NULL);
    strftime(date, sizeof(date), "%Y-%m-%d_%T", localtime(&now));
    snprintf(filename, sizeof(filename), "./results/%s_%s.csv", hostname, date);
    FILE *fp = fopen(filename, "w");
    if (fp == NULL) {
        fprintf(stderr, "Error: Failed to open file for writing.\n");
        return 1;
    }
    fprintf(fp, "GPU,Serial,Iteration,Time (us)\n");
    for (unsigned int j = 0; j < deviceCount; j++) 
    {
        for (int i = 0; i < loop_count; i++) 
        {
            fprintf(fp, "%u,%s,%d,%ld\n", j, serials[j], i + 1, time_diffs[j][i]);
        }
    }

    // Print statistics
    printf("\nSummary of nvmlDeviceGetTotalEnergyConsumption overhead (in microseconds):\n");
    for (unsigned int j = 0; j < deviceCount; j++) 
    {
        printf("GPU %u:\n", j);
        double avg, std;
        long min, max, q1, q3;

        compute_statistics(time_diffs[j], loop_count, &avg, &min, &max, &std, &q1, &q3);
        printf("Average time taken: %lf us\n", avg);
        printf("Minimum time taken: %ld us\n", min);
        printf("Maximum time taken: %ld us\n", max);
        printf("Standard deviation: %lf us\n", std);
        printf("Q1 (25th percentile): %ld us\n", q1);
        printf("Q3 (75th percentile): %ld us\n", q3);
        printf("\n");
    }

    // Clean up
    for (unsigned int j = 0; j < deviceCount; j++) {
        free(time_diffs[j]);
        free(serials[j]);
    }

    // Shutdown NVML library
    result = nvmlShutdown();
    check_nvml_error(result, "Failed to shutdown NVML");

    return 0;
}
