Testing out the time cost of the NVML function call nvmlDeviceGetTotalEnergyConsumption

(see https://docs.nvidia.com/deploy/nvml-api/group__nvmlDeviceQueries.html#group__nvmlDeviceQueries_1g732ab899b5bd18ac4bfb93c02de4900a)

To use, execute ./run.sh, you may change the number of executions there.
2 files are generated upon execution:
<ul>
    <li> under /logs, the file test_nvml_<em>date</em>.log with hostname, GPU system setup and executions' statistics (average, standard error, q1, q3, min, max)</li>
    <li> under /results, the file <em>hostname</em>_<em>date</em>.csv with all measurements in a CSV format. 
</ul>