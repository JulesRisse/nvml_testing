import pandas as pd
import matplotlib.pyplot as plt

# True to include min and max as separate points in the box plot
SHOW_MIN_MAX = False

data = pd.read_csv('summary.csv', delimiter=';')
gpus = data['Gpu']
mins = data['min']
q1 = data['Q1']
medians = data['avg']
q3 = data['Q3']
maxs = data['max']

gpus_data = []
for i in range(len(gpus)):
    if SHOW_MIN_MAX:
        gpus_data.append({
            'whislo': q1[i],
            'q1': q1[i],
            'med': medians[i],
            'q3': q3[i],
            'whishi': q3[i],
            'fliers': [mins[i], maxs[i]]
        })
    else:
        gpus_data.append([q1[i], medians[i], q3[i]])

fig, ax = plt.subplots(figsize=(10, 10), dpi=100)

for i, gpu_data in enumerate(gpus_data, start=1):
    if SHOW_MIN_MAX:
        ax.bxp([gpu_data], positions=[i], showfliers=True, widths=0.6,
               flierprops=dict(marker='o', color='red', markersize=5))
    else:
        ax.boxplot(gpu_data, positions=[i], vert=True, patch_artist=True,
                   widths=0.6, showfliers=False, whis=[0, 100])

ax.set_xticks(range(1, len(gpus) + 1))
ax.set_xticklabels(gpus)

ax.set_title('NVML Energy Query Latency by GPU', fontsize=16)
ax.set_ylabel('Latency (µs)', fontsize=12)

if SHOW_MIN_MAX:
    plt.savefig('boxplot_results_with_min_max.png')
else:
    plt.savefig('boxplot_results.png')

plt.show()
