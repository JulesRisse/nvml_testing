CC = gcc
CFLAGS = -I/usr/local/cuda/include
LDFLAGS = -L/usr/local/cuda/lib64/stubs/ -lnvidia-ml -lm

all: test_nvml

test_nvml: test_nvml.o
	$(CC) -o test_nvml test_nvml.o $(LDFLAGS)

test_nvml.o: test_nvml.c
	$(CC) $(CFLAGS) -c test_nvml.c

clean:
	rm -f test_nvml test_nvml.o
