#!/bin/bash

NUM_RUNS=10000

make clean
make
log_file="./logs/test_nvml_$(date +%Y-%m-%d_%T).log"
./test_nvml $NUM_RUNS | tee $log_file
